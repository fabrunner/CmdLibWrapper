// CmdLibWrapper.h

#pragma once
# include <string>
#include <vector>
class CmdLibWrapperPrivate;

//#ifdef BUILDINGCMDLIBWRAPPER
#define BYAEXPORT __declspec(dllexport)
//#else
//#define BYAEXPORT __declspec(dllimport)
//#endif

class BYAEXPORT CmdLibWrapper
{
private: CmdLibWrapperPrivate *_private;

public:
	CmdLibWrapper();
	
	//	CmdLibWrapper(bool logging, int mscDelayForDiscovery, std::string* deviceKey);
	CmdLibWrapper(bool logging, int mscDelayForDiscovery, std::vector<std::string*> deviceKey);
	
    ~CmdLibWrapper();

	bool AbortMotion(std::string* deviceKey);

	bool Open(std::string* deviceKey);

	void WriteLog(const char* outputText);

	void Shutdown(void);

	bool Close(std::string* deviceKey);

	bool GetIdentification(std::string* deviceKey, std::string* &identification);

	bool GetPosition(std::string* deviceKey, int motor, int &position);

	bool GetRelativeSteps(std::string* deviceKey, int motor, int &relativeSteps);

	bool RelativeMove(std::string* deviceKey, int motor, int relativeSteps);

	bool AbsoluteMove(std::string* deviceKey, int motor, int targetPos);

	bool JogNegative(std::string* deviceKey, int motor);

	bool JogPositive(std::string* deviceKey, int motor);

	bool StopMotion(std::string* deviceKey, int motor);

	bool GetMotionDone(std::string* deviceKey, int motor, bool &isMotionDone);

	bool SetCLEnabledSetting(std::string* deviceKey, int deviceAddress, int motor, int setting);

	int GetMasterDeviceAddress(std::string* deviceKey);

	int* GetDeviceAddresses(std::string* deviceKey);

	bool GetErrorMsg(std::string* deviceKey, std::string*& errMsg);

		//std::string* GetCapitalization(std::string* symbol);

    //std::string** GetValues(std::string* symbol, std::string* fields);
};
