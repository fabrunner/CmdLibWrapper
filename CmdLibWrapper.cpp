// This is the main DLL file.

#using ".\CmdLib.dll"

#include <msclr\auto_gcroot.h>
#include "CmdLibWrapper.h"
#include <iostream>
using namespace System::Runtime::InteropServices; // Marshal

class CmdLibWrapperPrivate
{
public: msclr::auto_gcroot<NewFocus::Picomotor::CmdLib8742^> cmdlib8742;
};

CmdLibWrapper::CmdLibWrapper()
{
	_private = new CmdLibWrapperPrivate();
	_private->cmdlib8742 = gcnew NewFocus::Picomotor::CmdLib8742();
}

CmdLibWrapper::CmdLibWrapper(bool logging, int mscDelayForDiscovery, std::vector<std::string*> deviceKey)
{
	std::cout << "constructor Wrapper";
	bool ^lgng = gcnew bool(logging);
	int ^msdld = gcnew int(mscDelayForDiscovery);
	_private = new CmdLibWrapperPrivate();
	array<System::String^>^ devKey = gcnew array<System::String^>(10);
	_private->cmdlib8742 = gcnew NewFocus::Picomotor::CmdLib8742((bool)lgng, (int)msdld, (System::String^%)devKey);
	for (int i=0;i<sizeof(devKey)/sizeof(std::string);i++)
		deviceKey.push_back((std::string*)Marshal::StringToHGlobalAnsi(devKey[i]).ToPointer());
}

bool CmdLibWrapper::AbortMotion(std::string* deviceKey)
{
    return _private->cmdlib8742->AbortMotion(gcnew System::String((char*)deviceKey));
}

bool CmdLibWrapper::Open(std::string* deviceKey)
{
	return _private->cmdlib8742->Open(gcnew System::String((char*)deviceKey));
}

void CmdLibWrapper::WriteLog(const char* outputText)
{
	return _private->cmdlib8742->WriteLog(gcnew System::String(outputText));
}

void CmdLibWrapper::Shutdown(void)
{
	return _private->cmdlib8742->Shutdown();
}

bool CmdLibWrapper::Close(std::string* deviceKey)
{
	return _private->cmdlib8742->Close(gcnew System::String((char*)deviceKey));
}

bool CmdLibWrapper::GetIdentification(std::string* deviceKey, std::string* &identification)
{
	System::String ^idnt = gcnew System::String((char*)identification);
	bool ret = _private->cmdlib8742->GetIdentification(gcnew System::String((char*)deviceKey), (System::String^%) idnt);
	identification = (std::string*)Marshal::StringToHGlobalAnsi(idnt).ToPointer();
	return ret;
}

bool CmdLibWrapper::GetPosition(std::string* deviceKey, int motor, int &position)
{
	int^ mot = gcnew int(motor);
	int^ pos = gcnew int;
	bool ret = _private->cmdlib8742->GetPosition(gcnew System::String((char*)deviceKey), (int)mot, (int%)pos);
	position = int((System::Int32)pos);
	return ret;
}

bool CmdLibWrapper::GetRelativeSteps(std::string* deviceKey, int motor, int &relativeSteps)
{
	int^ mot = gcnew int(motor);
	int^ relStp = gcnew int;
	bool ret = _private->cmdlib8742->GetRelativeSteps(gcnew System::String((char*)deviceKey), (int)mot, (int%)relStp);
	relativeSteps = int((System::Int32)relStp);
	return ret;
}

bool CmdLibWrapper::RelativeMove(std::string* deviceKey, int motor, int relativeSteps)
{
	int^ mot = gcnew int(motor);
	int^ relStp = gcnew int(relativeSteps);
	return _private->cmdlib8742->RelativeMove(gcnew System::String((char*)deviceKey), (int)mot, (int)relStp);
}

bool CmdLibWrapper::AbsoluteMove(std::string* deviceKey, int motor, int targetPos)
{
	int^ mot = gcnew int(motor);
	int^ trgPos = gcnew int(targetPos);
	return _private->cmdlib8742->AbsoluteMove(gcnew System::String((char*)deviceKey), (int)mot, (int)trgPos);
}

bool CmdLibWrapper::JogNegative(std::string* deviceKey, int motor)
{
	int^ mot = gcnew int(motor);
	return _private->cmdlib8742->JogNegative(gcnew System::String((char*)deviceKey), (int)mot);
}

bool CmdLibWrapper::JogPositive(std::string* deviceKey, int motor)
{
	int^ mot = gcnew int(motor);
	return _private->cmdlib8742->JogPositive(gcnew System::String((char*)deviceKey), (int)mot);
}

bool CmdLibWrapper::StopMotion(std::string* deviceKey, int motor)
{
	int^ mot = gcnew int(motor);
	return _private->cmdlib8742->StopMotion(gcnew System::String((char*)deviceKey), (int)mot);
}

bool CmdLibWrapper::GetMotionDone(std::string* deviceKey, int motor, bool &isMotionDone)
{
	int^ mot = gcnew int(motor);
	bool^ isMD = gcnew bool;
	bool ret = _private->cmdlib8742->GetMotionDone(gcnew System::String((char*)deviceKey), (int)mot, (bool%)isMD);
	isMotionDone = bool((System::Boolean)isMD);
	return ret;
}

bool CmdLibWrapper::SetCLEnabledSetting(std::string* deviceKey, int deviceAddress, int motor, int setting)
{
	int^ devAddr = gcnew int(deviceAddress);
	int^ mot = gcnew int(motor);
	int^ setng = gcnew int(setting);
	return _private->cmdlib8742->SetCLEnabledSetting(gcnew System::String((char*)deviceKey), (int)devAddr, (int)mot, (int)setng);
}

int CmdLibWrapper::GetMasterDeviceAddress(std::string* deviceKey)
{
	return _private->cmdlib8742->GetMasterDeviceAddress(gcnew System::String((char*)deviceKey));
}

int* CmdLibWrapper::GetDeviceAddresses(std::string* deviceKey)
{
	cli::array<int>^ managedValues = _private->cmdlib8742->GetDeviceAddresses(gcnew System::String((char*)deviceKey));
	int* unmanagedValues = new int[managedValues->Length];
	for (int i = 0; i < managedValues->Length; i++)
		unmanagedValues[i] = managedValues[i];
	return unmanagedValues;
}

bool CmdLibWrapper::GetErrorMsg(std::string* deviceKey, std::string*& errMsg)
{
	System::String ^emg = gcnew System::String("");
	bool ret = _private->cmdlib8742->GetErrorMsg(gcnew System::String((char*)deviceKey), (System::String^%) emg);
	errMsg = (std::string*)Marshal::StringToHGlobalAnsi(emg).ToPointer();
	return ret;
}

//std::string* CmdLibWrapper::GetCapitalization(std::string* symbol)
//{
//    System::String^ managedCapi = _private->cmdlib8742->GetCapitalization(gcnew System::String((char*)symbol));
//
//    return (std::string*)Marshal::StringToHGlobalAnsi(managedCapi).ToPointer();
//}
//
//std::string** CmdLibWrapper::GetValues(std::string* symbol, std::string* fields)
//{
//    cli::array<System::String^>^ managedValues = _private->cmdlib8742->GetValues(gcnew System::String((char*)symbol), gcnew System::String(fields));
//
//    std::string** unmanagedValues = new std::string*[managedValues->Length];
//
//    for (int i = 0; i < managedValues->Length; ++i)
//    {
//        unmanagedValues[i] = (std::string*)Marshal::StringToHGlobalAnsi(managedValues[i]).ToPointer();
//    }
//
//    return unmanagedValues;
//}

CmdLibWrapper::~CmdLibWrapper()
{
    delete _private;
}